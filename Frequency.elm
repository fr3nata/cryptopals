module Frequency exposing (..)

import Dict exposing (Dict)
import Char
import EditDistance
import Array


{-| scores a text for how likely it is to be english, lower is better
-}
score : String -> Int
score str =
    normalize str
        |> fromString
        |> common
        |> indexedHamming english


{-| Strips whitespace ond punctuation and converts all text to upppercase
-}
normalize : String -> String
normalize str =
    String.toUpper str |> String.filter (\c -> c /= ' ')


{-| char returns a frequency listing of all the characters
in a string
-}
fromString : String -> Dict Char Int
fromString str =
    chars (String.toList str) Dict.empty


chars : List Char -> Dict Char Int -> Dict Char Int
chars list dict =
    case list of
        [] ->
            dict

        head :: tail ->
            chars tail <| Dict.update head inc dict


inc : Maybe Int -> Maybe Int
inc n =
    case n of
        Just x ->
            Just (x + 1)

        Nothing ->
            Just 1


{-| common returns the most frequent 12 letters in the frequency list
-}
common : Dict Char Int -> String
common dict =
    Dict.toList dict
        |> List.sortBy Tuple.second
        |> List.reverse
        |> List.map Tuple.first
        |> List.map String.fromChar
        |> String.concat
        |> String.left 12


{-| hamming distance of two strings
-}
indexedHamming x y =
    let
        xs =
            String.toList x
                |> List.indexedMap (\i c -> ( c, i ))
                |> Dict.fromList

        ys =
            String.toList y
                |> List.indexedMap (\i c -> ( c, i ))
                |> Dict.fromList
    in
        Dict.merge
            (\k v out -> Dict.insert k (abs (v - 26)) out)
            (\k l r out -> Dict.insert k (abs (l - r)) out)
            (\k _ out -> Dict.insert k 26 out)
            xs
            ys
            Dict.empty
            |> Dict.toList
            |> List.map Tuple.second
            |> List.sum


english : String
english =
    "ETAOINSHRDLCUMWFGYPBVKJXQZ"
