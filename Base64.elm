module Base64 exposing (..)

import Char
import Bitwise exposing (..)
import Hex


fromAscii : String -> String
fromAscii =
    asciiToHex >> fromHex


toAscii : String -> String
toAscii =
    toHex >> hexToAscii


{-| convert ascii encoded string to a hex encoded string
-}
asciiToHex : String -> String
asciiToHex ascii =
    String.toList ascii
        |> List.map Char.toCode
        |> List.map Hex.toString
        |> List.map (String.padLeft 2 '0')
        |> String.concat


{-| convert hex to ascii
-}
hexToAscii : String -> String
hexToAscii hex =
    unconcatStr 2 hex
        |> List.map Hex.fromString
        |> List.map resToCode
        |> List.map Char.fromCode
        |> List.map String.fromChar
        |> String.concat


resToCode : Result String Int -> Int
resToCode res =
    case res of
        Ok i ->
            i

        Err _ ->
            0


{-| convert from a Hex encoded string to a Base64 encoded string
-}
fromHex : String -> String
fromHex hex =
    let
        hs =
            String.toLower hex
                |> String.toList
                |> List.filter Char.isHexDigit
                |> List.map parseHex
    in
        unconcat 3 hs
            |> List.map bitSplice
            |> String.concat
            |> padBase64


padBase64 : String -> String
padBase64 str =
    if String.length str % 4 == 0 then
        str
    else
        str ++ "="


parseHex : Char -> Int
parseHex c =
    if Char.isDigit c then
        Char.toCode c - 48
    else if Char.isHexDigit c then
        Char.toCode c - 87
    else
        -1


headOr : List a -> a -> a
headOr xs def =
    case List.head xs of
        Nothing ->
            def

        Just x ->
            x


hasHead : List a -> b -> b -> b
hasHead xs true false =
    case List.head xs of
        Just _ ->
            true

        Nothing ->
            false


bits6to4 : List Int -> List Int
bits6to4 b64s =
    let
        a64 =
            headOr b64s 0

        b64 =
            headOr (List.drop 1 b64s) 0

        a =
            shiftRightZfBy 2 a64

        b =
            or (shiftRightZfBy 4 b64) (shiftLeftBy 2 (and 3 a64))

        c =
            and 15 b64
    in
        a :: b :: c :: []



--bitSplice Lists of 3 hex integers to lists of 2 base64 integers


bitSplice : List Int -> String
bitSplice hexs =
    let
        a =
            headOr hexs 0

        b =
            headOr (List.drop 1 hexs) 0

        includeB =
            hasHead (List.drop 1 hexs) True False

        c =
            headOr (List.drop 2 hexs) 0

        padC =
            hasHead (List.drop 2 hexs) "" "="

        a64 =
            fromCode <|
                or (shiftLeftBy 2 a) (shiftRightZfBy 2 b)

        b64 =
            if includeB then
                fromCode <|
                    or (shiftLeftBy 4 (and 3 b)) c
            else
                ""
    in
        a64 ++ b64 ++ padC


toHex : String -> String
toHex str =
    let
        pads =
            String.indices "=" str |> List.length
    in
        String.dropRight pads str
            |> String.toList
            |> List.map toCode
            |> unconcat 2
            |> List.map bits6to4
            |> List.concat
            |> List.map Hex.toString
            |> String.concat
            |> trimOdd pads


trimOdd : Int -> String -> String
trimOdd pads str =
    if pads > 0 then
        if (String.length str) % 2 == 0 then
            String.dropRight 2 str
        else
            String.dropRight 1 str
    else
        str


base64 : String
base64 =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"


fromCode : Int -> String
fromCode n =
    String.slice n (n + 1) base64


toCode : Char -> Int
toCode c =
    String.indices (String.fromChar c) base64
        |> List.head
        |> Maybe.withDefault 0



-- Utility


unconcat : Int -> List a -> List (List a)
unconcat n list =
    case list of
        [] ->
            []

        _ ->
            let
                front =
                    List.take n list

                back =
                    unconcat n <| List.drop n list
            in
                front :: back


unconcatStr : Int -> String -> List String
unconcatStr n list =
    case list of
        "" ->
            []

        _ ->
            let
                front =
                    String.left n list

                back =
                    unconcatStr n <| String.dropLeft n list
            in
                front :: back


trimPad : String -> String -> String
trimPad pad str =
    if String.endsWith pad str then
        trimPad pad <| String.dropRight 1 str
    else
        str
