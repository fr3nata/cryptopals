module Set1 exposing (..)

import Base64 exposing (..)
import Xor
import Decrypt
import Html exposing (..)
import Html.Attributes exposing (placeholder, autofocus, readonly, size, value, id)
import Html.Events exposing (onInput)


main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Model =
    { asciiString : String
    , hexString : String
    , base64String : String
    , xorA : String
    , xorB : String
    , repeatCode : String
    , repeatKey : String
    , cipher : String
    , multiCipher : String
    }


init =
    ( { asciiString = ""
      , hexString = ""
      , base64String = ""
      , xorA = ""
      , xorB = ""
      , repeatCode = ""
      , repeatKey = ""
      , cipher = ""
      , multiCipher = ""
      }
    , Cmd.none
    )


type Msg
    = NewHexString String
    | NewAsciiString String
    | NewBase64String String
    | NewXorA String
    | NewXorB String
    | NewRepeatCode String
    | NewRepeatKey String
    | NewCipher String
    | NewMultiCipher String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewHexString hex ->
            ( { model
                | hexString = hex
                , base64String = fromHex hex
                , asciiString = (fromHex >> toAscii) hex
              }
            , Cmd.none
            )

        NewAsciiString ascii ->
            ( { model
                | asciiString = ascii
                , base64String = fromAscii ascii
                , hexString = (fromAscii >> toHex) ascii
              }
            , Cmd.none
            )

        NewBase64String base64 ->
            ( { model
                | base64String = base64
                , hexString = Base64.toHex base64
                , asciiString = Base64.toAscii base64
              }
            , Cmd.none
            )

        NewXorA a ->
            ( { model | xorA = a }, Cmd.none )

        NewXorB b ->
            ( { model | xorB = b }, Cmd.none )

        NewRepeatCode c ->
            ( { model | repeatCode = c }, Cmd.none )

        NewRepeatKey k ->
            ( { model | repeatKey = k }, Cmd.none )

        NewCipher c ->
            ( { model | cipher = c }, Cmd.none )

        NewMultiCipher c ->
            ( { model | multiCipher = c }, Cmd.none )


view : Model -> Html Msg
view model =
    div []
        [ viewEncodings model
        , viewFixedXor model
        , viewRepeatXor model
        , viewCipher model
        , viewMultiCipher model
        ]


viewFixedXor model =
    div [ id "fixedXor" ]
        [ h4 [] [ text "Fixed Xor" ]
        , input
            [ onInput NewXorA
            , placeholder "Hex encoded string"
            , size 100
            , value model.xorA
            ]
            []
        , input
            [ onInput NewXorB
            , placeholder "Hex encoded string"
            , size 100
            , value model.xorB
            ]
            []
        , input
            [ placeholder "Xor result, hex encoded string"
            , size 100
            , value (Xor.hex model.xorA model.xorB)
            , readonly True
            ]
            []
        , input
            [ placeholder "Xor result, ascii encoded string"
            , size 100
            , value <| Base64.hexToAscii (Xor.hex model.xorA model.xorB)
            , readonly True
            ]
            []
        ]


viewEncodings : Model -> Html Msg
viewEncodings model =
    div
        [ id "encodings" ]
        [ h4 [] [ text "Encodings" ]
        , input
            [ onInput NewAsciiString
            , autofocus True
            , placeholder "ASCII Encoded String"
            , size 100
            , value model.asciiString
            ]
            []
        , input
            [ onInput NewHexString
            , placeholder "Hex Encoded String"
            , size 100
            , value model.hexString
            ]
            []
        , input
            [ onInput NewBase64String
            , placeholder "Base64 Encoded String"
            , size 100
            , value model.base64String
            ]
            []
        ]


viewRepeatXor : Model -> Html Msg
viewRepeatXor model =
    div [ id "repeatXor" ]
        [ h4 [] [ text "Repeating Xor" ]
        , input
            [ onInput NewRepeatCode
            , placeholder "Hex Encoded String"
            , size 100
            , value model.repeatCode
            ]
            []
        , input
            [ onInput NewRepeatKey
            , placeholder "Repeating Key, ASCII"
            , size 100
            , value model.repeatKey
            ]
            []
        , input
            [ placeholder "XOR result, hex encoded"
            , size 100
            , value (Base64.asciiToHex (Xor.repeat model.repeatKey (Base64.hexToAscii model.repeatCode)))
            , readonly True
            ]
            []
        ]


viewCipher : Model -> Html Msg
viewCipher model =
    let
        alphabet =
            "abcdefghijklmnopqrstuvwxyz"

        keys =
            (String.toList alphabet)
                ++ (String.toUpper alphabet |> String.toList)
                |> List.map String.fromChar
    in
        div [ id "cipher" ]
            [ h4 [] [ text "Decrypting single key ciphers" ]
            , input
                [ onInput NewCipher
                , placeholder "Cipher, hex encoded"
                , size 100
                , value model.cipher
                ]
                []
            , input
                [ placeholder "Best decryption candidate, ASCII"
                , size 100
                , value (Decrypt.decrypt (Base64.hexToAscii model.cipher) keys)
                , readonly True
                ]
                []
            ]


viewMultiCipher : Model -> Html Msg
viewMultiCipher model =
    let
        alphabet =
            "abcdefghijklmnopqrstuvwxyz"

        digits =
            "0123456789"

        keys =
            (alphabet ++ (String.toUpper alphabet) ++ digits)
                |> String.toList
                |> List.map String.fromChar

        codes =
            String.split "\n" model.multiCipher
                |> List.map Base64.hexToAscii
    in
        div [ id "multi-cipher" ]
            [ h4 [] [ text "Find the cipher" ]
            , textarea
                [ onInput NewMultiCipher
                , placeholder "Ciphers, hex encoded"
                , size 100
                , value model.multiCipher
                ]
                []
            , input
                [ placeholder "Best decryption candidate, ASCII"
                , size 100
                , value (Decrypt.decryptMulti codes keys)
                , readonly True
                ]
                []
            ]


subscriptions model =
    Sub.none
