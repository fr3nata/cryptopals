module Base64Test exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Base64
import Debug


suite : Test
suite =
    describe "Base64 encoding"
        [ describe "fromAscii"
            [ test "from spec" <|
                \_ ->
                    let
                        ascii =
                            "any carnal pleasure."

                        base64 =
                            "YW55IGNhcm5hbCBwbGVhc3VyZS4="
                    in
                        Expect.equal base64 (Base64.fromAscii ascii)
            ]
        , describe "fromHex"
            [ test "cryptopals" <|
                \_ ->
                    let
                        hex =
                            "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"

                        base64 =
                            "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
                    in
                        Expect.equal base64 (Base64.fromHex hex)
            ]
        , describe "toAscii >> fromAscii is identity"
            [ fuzz string "toAscii reverses fromAscii" <|
                \rand ->
                    rand
                        --|> Debug.log "fuzzed string"
                        |> (Base64.fromAscii >> Base64.toAscii)
                        |> (==) rand
                        |> Expect.equal True
            ]
        ]
