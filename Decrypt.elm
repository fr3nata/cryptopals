module Decrypt exposing (..)

import Xor
import Frequency


--TODO create record type for (PlainText, Score, Key) candidate decryptions


{-| attempts to decrypt with all keys and returns the best candidate for engilsh text
-}
decrypt : String -> List String -> String
decrypt code keys =
    candidates code keys
        |> Tuple.first


decryptMulti : List String -> List String -> String
decryptMulti codes keys =
    List.map (flip candidates keys) codes
        |> List.sortBy Tuple.second
        |> List.map (Debug.log "candidate: ")
        |> List.head
        |> Maybe.withDefault ( "", 0 )
        |> Tuple.first


candidates : String -> List String -> ( String, Int )
candidates code keys =
    let
        possible =
            List.map (\key -> decryptOne code key) keys
                |> List.sortBy Tuple.second
    in
        case possible of
            [] ->
                ( "", 0 )

            h :: t ->
                h


decryptOne : String -> String -> ( String, Int )
decryptOne code key =
    let
        plain =
            Xor.repeat key code

        score =
            Frequency.score plain
    in
        ( plain, score )
