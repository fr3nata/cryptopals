module XorTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Xor
import Base64
import Debug


suite : Test
suite =
    describe "Xor testing"
        [ describe "fixed Xor"
            [ test "cryptopals" <|
                \_ ->
                    let
                        a =
                            "1c0111001f010100061a024b53535009181c"

                        b =
                            "686974207468652062756c6c277320657965"

                        result =
                            "746865206b696420646f6e277420706c6179"
                    in
                        Expect.equal result (Xor.hex a b)
            , fuzz string "any string XORed against itself is 0" <|
                \rand ->
                    rand
                        --|> Debug.log "fuzzed string"
                        |> Xor.ascii rand
                        |> Expect.equal (String.repeat (String.length rand) "\x00")
            ]
        , describe "repeating Xor"
            [ test "cryptopals" <|
                \_ ->
                    let
                        c =
                            "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736" |> Base64.hexToAscii

                        k =
                            "X"

                        result =
                            "Cooking MC's like a pound of bacon"
                    in
                        Expect.equal result (Xor.repeat k c)
            , fuzz string "XORing against zero does not change input" <|
                \rand ->
                    rand
                        --|> Debug.log "fuzzed string"
                        |> Xor.repeat "\x00"
                        |> Expect.equal rand
            ]
        ]
